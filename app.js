const express = require('express'),
	http = require('http'),
	path = require('path'),
	app = express(),
	server = http.createServer(app),
	solver = require('./solver')

app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
	res.sendfile(__dirname + '/solver.html')
})

app.get('/solve', (req, res) => {
	const {letters} = req.query
	res.send(JSON.stringify(solver.solveSanajahti(letters)))
})

server.listen(3000, () => {
	console.log('Sanajuhta server listening at 3000')
})

