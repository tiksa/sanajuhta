# Sanajuhta solver

This project is a solver for the word-finding game called Sanajahti/Wordament/Scrabble. This version finds all Finnish words in the given letter grid. It uses [kotus](http://kaino.kotus.fi/sanat/nykysuomi/) list of Finnish words as a source of accepted words.

The actual solving magic happens at `solver.js` which finds all Finnish words in any N-dimensional cube.

# Prerequisities

Node.js, npm

# Setup

`npm i`

# Running

Type `npm start`, and go to `localhost:3000` and fill in the grid by typing some letters!

You can also try the solver.js with various inputs like:

`npm run solve <letters> <cube_width>`

`npm run solve tääoneliö 3`

Or then, as an example run:
`npm run solver-example`

# Screenshots

![Sanajuhta](screenshot.png)
