const {getCell, findPermutations, findUnvisitNeighbors, createCube} = require('./solver')

describe('getCell', () => {
  test('returns correct cell with 1 dimension', () => {
    expect(getCell([1,2,3], [2])).toEqual(3)
  })

  test('returns correct cell with 2 dimensions', () => {
    expect(getCell([[1,2,3], [4,5,6], [7,8,9]], [2,0])).toEqual(7)
  })
})

describe('findPermutations', () => {
  test('returns correct permutations', () => {
    expect(findPermutations([[1,2], [3,4,5], [9]])).toEqual([[1,3,9], [1,4,9], [1,5,9], [2,3,9], [2,4,9], [2,5,9]])
  })
})

describe('createCube', () => {
  test('creates two-dimensional "cube" correctly', () => {
    expect(createCube('abcdefghi', 3)).toEqual([
      [cell('a', [0,0]), cell('b', [0,1]), cell('c', [0,2])],
      [cell('d', [1,0]), cell('e', [1,1]), cell('f', [1,2])],
      [cell('g', [2,0]), cell('h', [2,1]), cell('i', [2,2])]])
  })
})

describe('findUnvisitNeighbors', () => {
  test('finds neighbors in all directions', () => {
    const cube = createCube('abcdefghi', 3)
    const c = {coords: [1,1]}
    expect(findUnvisitNeighbors(cube, c)).toEqual([
      cell('a', [0,0]), cell('b', [0,1]), cell('c', [0,2]),
      cell('d', [1,0]), cell('f', [1,2]), cell('g', [2,0]),
      cell('h', [2,1]), cell('i', [2,2])])
  })

  test('takes boundaries into account', () => {
    const cube = createCube('abcdefghi', 3)
    const c = {coords: [0,1]}
    expect(findUnvisitNeighbors(cube, c)).toEqual([
      cell('a', [0,0]), cell('c', [0,2]), cell('d', [1,0]),
      cell('e', [1,1]), cell('f', [1,2])])
  })
})

function cell(letter, coords, visit = false) {
  return {
    letter,
    coords,
    visit
  }
}