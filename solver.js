const _ = require('lodash'),
  fs = require('fs'),
  Trie = require('node-trie')

const data = fs.readFileSync('./dict.txt', 'utf8'),
  defaultDict = data.split('\n')

export function solve(letterStr, size, dict = defaultDict) {
  // index all words to a trie data structure
  const trie = new Trie()
  dict.forEach(word => trie.add(word, word))

  // transform the string of letters to an N-dimensional cube object
  const cube = createCube(letterStr, size)

  return _.chain(cube)
    .flattenDeep()
    .reduce((totalWords, cell) => {
      // starting from each cell, perform a DFS-like search for words and concat the found ones
      const foundWords = search(cube, trie, getCell(cube, cell.coords), '')
      return totalWords.concat(foundWords)
    }, [])
    .uniq()
    .sortBy(word => -word.length)
    .value()
}

export function findUnvisitNeighbors(cube, {coords}) {
  const ranges = coords.map(findNeighborCoords(cube))

  return findPermutations(ranges)
    .map(p => getCell(cube, p))
    .filter(c => !isSameCoords(c.coords, coords) && !c.visit)
}

export function findPermutations(ranges, coords = []) {
  if (!ranges.length)
    return [coords]

  return _.flatMap(_.first(ranges), c => findPermutations(_.drop(ranges), coords.concat(c)))
}

export function createCube(letterStr, size, coords = []) {
  const dim = getBaseLog(size, letterStr.length)
  if (dim < 1)
    return { coords, letter: letterStr, visit: false }

  return _.range(size).map((c, index) => {
    const nextLen = Math.pow(size, dim - 1)
    return createCube(letterStr.substring(index * nextLen, (index + 1) * nextLen), size, coords.concat(index))
  })
}

function search(cube, trie, cell, str) {
  str += cell.letter
  const potentialMatches = trie.get(str) || []
  if (!potentialMatches.length)
    return []

  const clonedCube = clone(cube)
  getCell(clonedCube, cell.coords).visit = true

  const currentStrMatch = potentialMatches[0] === str && str
  const neighbors = findUnvisitNeighbors(clonedCube, cell)
  const neighborMatches = _.flatMap(neighbors, neighbor => search(clonedCube, trie, neighbor, str))

  return neighborMatches.concat(currentStrMatch || [])
}

const isSameCoords = (a, b) => a.every((elem, index) => elem === b[index])

const clone = cube => cube.length && cube.map(clone) || {...cube}

const getBaseLog = (x, y) => Math.log(y) / Math.log(x)

const findNeighborCoords = cube => c => _.range(Math.max(0, c - 1), Math.min(c + 1, cube.length - 1) + 1)

export const getCell = (cube, coords) => coords.reduce((cube, coord) => cube[coord], cube)

// specific export function for Sanajuhta UI
export function solveSanajahti(letters, dict = defaultDict) {
  return solve(letters, 4, dict)
}

const args = process.argv
if (args.length === 4) {
  const [, , letters, size] = args
  console.log(`Solving cube with letters ${letters}`)
  console.log(solve(letters, size).join('\n'))
}